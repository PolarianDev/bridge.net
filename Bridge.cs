namespace Bridge
{
    class Bridge
    {
        public string ircChannel { get; }
        public long discordChannel { get; }

        public Bridge(string ircChannel, long discordChannel)
        {
            this.ircChannel = ircChannel;
            this.discordChannel = discordChannel;
        }
    }
}
