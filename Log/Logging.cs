using System.IO;
using System;

namespace Bridge.Log
{
    public enum LoggerType
    {
        File, Console, Both
    }

    public enum LoggerLevel
    {
        Debug, Info, Warning, Error
    }

    public class Logger
    {
        private LoggerType _type;
        private LoggerLevel _level;
        private TextWriter _writer;

        public Logger(string path, LoggerType type, LoggerLevel level)
        {
            this._type = type;
            this._level = level;
            bool existed = false;
            if (File.Exists(path))
            {
                existed = true;
            }
            this._writer = TextWriter.Synchronized(File.AppendText(path));
            if (existed)
            {
                _writer.WriteLine("\n**** New Session ****\n");
            }
        }

        private bool CheckLogLevel(LoggerLevel level)
        {
            return ((int)this._level <= (int)level);
        }

        private void Log(string content)
        {
            switch (_type)
            {
                case LoggerType.Both:
                    Console.WriteLine(content);
                    _writer.WriteLine(content);
                    _writer.Flush();
                    break;

                case LoggerType.Console:
                    Console.WriteLine(content);
                    break;

                case LoggerType.File:
                    _writer.WriteLine(content);
                    _writer.Flush();
                    break;
            }
            Console.ResetColor();
        }

        public void Debug(string msg)
        {
            if (CheckLogLevel(LoggerLevel.Debug))
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Log("[Debug]: " + msg);
            }
        }

        public void Info(string msg)
        {
            if (CheckLogLevel(LoggerLevel.Info))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Log("[Info]: " + msg);
            }
        }

        public void Warn(string msg)
        {
            if (CheckLogLevel(LoggerLevel.Warning))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Log("[Warning]: " + msg);
            }
        }

        public void Error(string msg)
        {
            if (CheckLogLevel(LoggerLevel.Error))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Log("[Error]: " + msg);
            }
        }
    }
}
