# Bridge.Net
Bridge.Net is a Discord/IRC bridge written in C#

### Bridge.Net Status [2021/12/30]
Bridge.net is still undergoing a rewrite in the [rewrite branch](https://gitlab.com/PolarianDev/bridge.net/-/tree/rewrite).

However, the rewrite is mostly ready to be merged into development, and will be tested to see if there is any bugs. However due to the simplified codebase, and furthermore better code quality, it is more likely to be stable, and furthermore would be more stable than the outdated codebase.

Happy New Year,
Polarian

## Contributing
Development has been moved from master to development branch because master is now stable

## Project structure
config directory: The config directory contains the default config file for the bot

## Setup and installation
As development is still ongoing, if you would like to use the bridge during development, clone the repo and compile the code, making sure to copy over the config from the config directory to the root directory. This should not be attempted unless you know what you are doing, installation guide will be provided once the bridge has been finished

## Errors/Bugs
If you experience any errors or bugs, please enable Debug mode in the config, when experience the error again, please submit an issue [here](https://gitlab.com/PolarianDev/bridge.net/-/issues) attaching `Latest.log` and the steps to reproduce the error, or reach out to me on discord `Polarian#7560`
